package pl.codementors.table;

import java.util.Scanner;

/**
 * Calculates the sum and the product of ten entered integers.
 *
 * @author Paweł Wieraszka
 */
public class Table {
    public static void main(String[] args){

        int[] table = new int[10];
        Scanner input = new Scanner(System.in);
        System.out.println("Enter 10 integers in a row:");
        int sum = 0;
        double product = 1;

        for (int i = 0; i < 10; i++) {
            table[i] = input.nextInt();
            sum += table[i];
            product *= table[i];
        }

        System.out.print("Your numbers are: ");

        for (int t : table) {
            System.out.print(t+" ");
        }

        System.out.println("\nSum of your numbers is: "+sum+"\nProduct of your numbers is: "+product);
    }
}
